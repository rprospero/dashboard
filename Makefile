PROGS=cairo-demo-xcb

MYCFLAGS=`pkg-config --cflags cairo x11`
MYLDFLAGS=`pkg-config --libs cairo x11`

all: $(PROGS)

%.o: %.c
	$(CXX) -lpthread -lm -c $(CFLAGS) $(CPPFLAGS) ${MYCGLAGS} $< -o $@

%: %.c
	$(CXX) -lpthread $(CFLAGS) $(CPPFLAGS) ${MYCFLAGS} ${MYLDFLAGS} $^ -o $@

clean:
	rm -f $(PROGS) *.o
