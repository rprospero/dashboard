let
  pkgs = import <nixpkgs> {};
in
  pkgs.mkShell {
    buildInputs = [
        pkgs.cairo
        pkgs.cairo
        pkgs.xorg.libX11
        pkgs.xorg.libX11.man
        pkgs.pkg-config
        pkgs.manpages
    ];
  }
